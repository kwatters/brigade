package com.kmwllc.brigade.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.stream.Stream;

public class CRBufferedReader extends BufferedReader {

  
  // private Reader reader;
  
  @Override
  public int read() throws IOException {
    // TODO Auto-generated method stub
    return super.read();
  }

  @Override
  public int read(char[] arg0, int arg1, int arg2) throws IOException {
    // TODO Auto-generated method stub
    return super.read(arg0, arg1, arg2);
  }

  @Override
  public long skip(long arg0) throws IOException {
    // TODO Auto-generated method stub
    return super.skip(arg0);
  }

  @Override
  public boolean ready() throws IOException {
    // TODO Auto-generated method stub
    return super.ready();
  }

  @Override
  public void mark(int arg0) throws IOException {
    // TODO Auto-generated method stub
    super.mark(arg0);
  }

  @Override
  public void reset() throws IOException {
    // TODO Auto-generated method stub
    super.reset();
  }

  @Override
  public void close() throws IOException {
    // TODO Auto-generated method stub
    super.close();
  }

  @Override
  public Stream<String> lines() {
    // TODO Auto-generated method stub
    return super.lines();
  }

  @Override
  public int read(CharBuffer arg0) throws IOException {
    // TODO Auto-generated method stub
    return super.read(arg0);
  }

  @Override
  public int read(char[] arg0) throws IOException {
    // TODO Auto-generated method stub
    return super.read(arg0);
  }

  @Override
  public String readLine() throws IOException {
    // TODO Auto-generated method stub
    
    // return super.readLine();
   return readUntilNewline();
  }

  public CRBufferedReader(Reader reader) {
    super(reader);
    // TODO Auto-generated constructor stub
  }

  private String readUntilNewline() throws IOException {
    
    StringBuilder sb = new StringBuilder(1024);
    for (int c = this.read(); (c > -1) && (c != 13); c = this.read()) {
      // System.out.print((char) c);
      if (c == -1) {
        // end of the stream.
        return sb.toString();
      }
      if (c == 10) {
        c = 32;
        // continue;
      }
      sb.append((char) c);
    }
    // System.out.println("READLINE" + sb.toString());
    
    //if (sb.length() == 0) {      
      // System.out.println("Empty row found!");
    //  return readUntilNewline();
    //}
    //return ((sb.length() > 0) ? sb.toString() : null);
    return sb.toString();
  }
  
}
